#include <iostream>

struct Employee
{
	short id;
	int age;
	double wage;
};

void printInformation(Employee employee)
{
	std::cout << "ID:   " << employee.id << "\n";
	std::cout << "Age:  " << employee.age << "\n";
	std::cout << "Wage: " << employee.wage << "\n";
}

int main()
{
	Employee joe = { 14, 32, 24.15 };
	Employee frank = { 15, 28, 18.27 };

	// Print Joe's information
	printInformation(joe);

	std::cout << "\n";

	// Print Frank's information
	printInformation(frank);

	return 0;
}

/*
#include <iostream>

struct Point3d
{
double x;
double y;
double z;
};

Point3d getZeroPoint()
{
Point3d temp = { 0.0, 0.0, 0.0 };
return temp;
}

int main()
{
Point3d zero = getZeroPoint();

if (zero.x == 0.0 && zero.y == 0.0 && zero.z == 0.0)
std::cout << "The point is zero\n";
else
std::cout << "The point is not zero\n";

return 0;
}
*/