#include <iostream>
struct A {
	A(int i = 0, double j = 0.0) : i(i), j(j) {}

	int i;
	double j;
};

int main()
{
	A a;            // OK (C++11/14), a.i is 0 and a.j is 0.0
	A b = {};       // OK (C++11/14), b.i is 0 and b.j is 0.0
	A c = { 1 };      // OK (C++11/14), c.i is 1 and c.j is 0.0
	A d = { 1, 2.0 }; // OK (C++11/14), d.i is 1 and d.j is 2.0
	std::cout << d.i << std::endl;
}