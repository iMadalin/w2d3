#include <iostream>
struct Triangle
{
	double length = 1.0;
	double width = 1.0;
};

int main()
{
	Triangle x; 

	std::cout << x.length << std::endl;
	x.length = 2.0; 

	Triangle y{ 2.0, 2.0 }; // uniform initialization 
	std::cout << y.length << std::endl;
	return 0;
}